# -*- coding: utf-8 -*-
import base64
import io
import dash
from dash import dcc
from dash import html
from dash import dash_table
import plotly.graph_objs as go
import pandas as pd
import pathlib

from dash.dependencies import Input, Output, State
from scipy import stats

group_colors = {"control": "light blue", "reference": "red"}

app = dash.Dash(
    __name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}]
    # __name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}],url_base_pathname='/PopGen/'
)
## when using reverse proxy
# https://community.plotly.com/t/proxy-routing-difficulties-on-dash-deployment/20387

server = app.server

PATH = pathlib.Path(__file__).parent
DATA_PATH = PATH.joinpath("data").resolve()
default_study_data = pd.read_csv(DATA_PATH.joinpath("prevalence.csv"),index_col=0)
#default_dos_data = ""
default_dos_data = pd.read_csv(DATA_PATH.joinpath("dosage.csv"),index_col=0)
default_annotation_data = pd.read_csv(DATA_PATH.joinpath("annotation.csv"))

# App Layout
app.layout = html.Div(
    children=[
    	dcc.Location(id='url', refresh=True),
        # Error Message
        html.Div(id="error-message"),
        html.Div(id="error-message-url"),
        # Top Banner
        html.Div(
            className="study-browser-banner row",
            children=[
                html.H2(className="h2-title", children="PopGen"),
                html.Div(
                    className="div-logo",
                    children=html.Img(
                        className="logo", src=app.get_asset_url("dash-logo-new.png")
                    ),
                ),
                html.H2(className="h2-title-mobile", children="PopGen"),
            ],
        ),
        # Body of the App
        html.Div(
            className="row app-body",
            children=[
                # User Controls
                html.Div(
                    className="four columns card",
                    children=[
                        html.Div(
                            className="bg-white-h800 user-control",
                            children=[
                                html.Div(
                                    className="padding-top-bot",
                                    children=[
                                        html.H6('Minimum Prevalence'),
                                        dcc.Slider(
                                        	id="min-prevalence",
                                            min=0,
                                            max=50,
                                            marks={i: '{}'.format(i) if i == 1 else str(i) for i in range(0, 50, 10)},
                                            value=20,
                                            tooltip={'always_visible': False},
                                        ),
                                    ],
                                ),
                                html.Div(
                                    className="padding-top-bot",
                                    children=[
                                        html.H6("Choose the type of plot"),
                                        dcc.RadioItems(
                                            id="chart-type",
                                            options=[
                                                {"label": "Line", "value": "line"},
                                                {"label": "Heatmap", "value": "heatmap"},
                                            ],
                                            value="line",
                                            labelStyle={
                                                "display": "inline-block",
                                                "padding": "4px 12px 12px 0px",
                                            },
                                        ),
                                        dcc.Checklist(
                                            id="display-legend",
                                            options=[{'label': 'Show Legend', 'value': 'legendon'}],
                                            value=[],
                                        ),  
                                    ],
                                ),
                                html.Div(
                                    className="padding-top-bot",
                                    children=[
                                        html.H6("Prevalence of Mutations CSV/EXCEL File"),
                                        dcc.Upload(
                                            id="upload-data",
                                            className="upload",
                                            children=html.Div(
                                                children=[
                                                    html.P("Drag and Drop or "),
                                                    html.A("Select Files"),
                                                ]
                                            ),
                                            accept=".csv,.xlsx",
                                        ),
                                    ],
                                ), 
                                html.Div(
                                    className="padding-top-bot",
                                    children=[
                                        html.H6("Dosage CSV/EXCEL File"),
                                        dcc.Upload(
                                            id="upload-dosage-data",
                                            className="upload",
                                            children=html.Div(
                                                children=[
                                                    html.P("Drag and Drop or "),
                                                    html.A("Select Files"),
                                                ]
                                            ),
                                            accept=".csv,.xlsx",
                                        ),
                                    ],
                                ),
                                html.Div(
                                    className="padding-top-bot",
                                    children=[
                                        html.H6("Annotation CSV/EXCEL File"),
                                        dcc.Upload(
                                            id="upload-annotation-data",
                                            className="upload",
                                            children=html.Div(
                                                children=[
                                                    html.P("Drag and Drop or "),
                                                    html.A("Select Files"),
                                                ]
                                            ),
                                            accept=".csv,.xlsx", 
                                        ),
                                    ],
                                ),
                            ],
                        )
                    ],
                ),
                # Graph
                html.Div(
                    className="eight columns card-left",
                    children=[
                        html.Div(
                            className="bg-white-h800",
                            children=[
                                html.H5("The Prevalence of Mutations"),
                                dcc.Graph(id="plot"),
                                html.H5("Drug Dosage Fold Change"),
                                dcc.Graph(
                                	id="dosage",
                                	style={"height":"300px"},
                                )
                            ],
                        )
                   
                    ],
                ),
                html.Div(
                    className="twelve columns card-page",
                    children=[
                        html.Div(
                            className="bg-white",
                            children=[
                            	html.H5("Annotations",style={'padding-left':' 0px',}),
                            	dash_table.DataTable(
                            	    id='datatable-annotation',
                            	    editable=True,
                            	    sort_action="native",
                            	    #row_selectable="multi",
                            	    page_action="native",
                                    page_current= 0,
                                    page_size= 12,
                                    export_format='xlsx',
                                    export_headers='display',
                                    filter_action='native',
                                    style_table={
                                    	
                                    },
                            	    style_data={
                                        'whiteSpace': 'normal',
                                        'height': 'auto',
                                        #'width': '96%',
                                        #'maxHeight': '300px',
                                        #'overflowX': 'scroll',
                                        #'overflow': 'hidden',
                                    },
                                    style_data_conditional=[
                                        {
                                            'if': {'row_index': 'odd'},
                                            'backgroundColor': 'rgb(248, 248, 248)'
                                        }
                                    ],
                                    style_header={
                                        'backgroundColor': 'rgb(230, 230, 230)',
                                        'fontWeight': 'bold'
                                    }
                            	),
                            ],
                        )
                    ]
                ),
             
             	html.Div(id='none',children=[],style={'display': 'none'}),
                # Hidden div inside the app that stores the intermediate value
                html.Div(id='intermediate-value', style={'display': 'none'}),
                dcc.Store(id="error", storage_type="memory"),
                dcc.Store(id="error-url", storage_type="memory"),
            ],
        ),
    ]
)


@app.callback(
    [
        Output("error-url", "data"),
        Output("error-message-url", "children"),
    ],
    [Input('url', 'pathname')]
)
def read_default_file(pathname): 
    global default_study_data
    global default_dos_data
    global default_annotation_data
    error_status = False
    error_message = None
    
    if pathname != '/':
        proj=pathname.replace('/','').replace('PopGen','')
        try:
            default_study_data = pd.read_csv(DATA_PATH.joinpath(proj).joinpath("prevalence.csv"),index_col=0)
         # Data is invalid
        except Exception as e:
            error_message = html.Div(
                className="alert",
                children=["This doesn't seem to be a valid URL!"],
            )
            error_status = True
            default_study_data = pd.DataFrame() 
        
        try: 
            default_dos_data = pd.read_csv(DATA_PATH.joinpath(proj).joinpath("dosage.csv"),index_col=0)
        except Exception as e:
            error_message = html.Div(
            className="alert",
                children=["This doesn't seem to be a valid URL!"],
            )
            error_status = True
            default_dos_data = pd.DataFrame() 
            
        try:
            default_annotation_data = pd.read_csv(DATA_PATH.joinpath(proj).joinpath("annotation.csv"))
        except Exception as e:
            error_message = html.Div(
            className="alert",
                children=["This doesn't seem to be a valid URL!"],
            )
            error_status = True
            default_annotation_data = pd.DataFrame()
    
    return error_status, error_message
        
# Callback to generate error message
# Also sets the data to be used
# If there is an error use default data else use uploaded data
@app.callback(
    [
        Output("error", "data"),
        Output("error-message", "children"),
    ],
    [   
        Input("upload-data", "contents"),
        Input("upload-data", "filename"),
        Input("upload-dosage-data", "contents"),
        Input("upload-dosage-data", "filename"),
        Input('upload-annotation-data', 'contents'),
        Input('upload-annotation-data', 'filename')
    ],
)
def update_error(contents,datafilename, doscontents, dosagefilename, anncontents, annfilename):

    error_status = False
    error_message = None
    study_data = default_study_data
    dosage_data = default_dos_data
    ann_data = default_annotation_data

    # Check if there is uploaded content
    if contents:
        content_type, content_string = contents.split(",")
        decoded = base64.b64decode(content_string)

        # Try reading uploaded file
        try:
            if 'csv' in datafilename:
                study_data = pd.read_csv(io.StringIO(decoded.decode("utf-8")))
            elif 'xls' in datafilename:
                study_data = pd.read_excel(io.BytesIO(decoded))

           # missing_columns = {
           #     "group_id",
           #     "group_type",
           #     "reading_value",
           #     "study_id",
           # }.difference(study_data.columns)

           # if missing_columns:
           #     error_message = html.Div(
           #         className="alert",
           #         children=["Missing columns: " + str(missing_columns)],
           #     )
           #     error_status = True
           #     study_data = default_study_data

        # Data is invalid
        except Exception as e:
            error_message = html.Div(
                className="alert",
                children=["That doesn't seem to be a valid prevalnace csv/EXCEL file!"],
            )
            error_status = True
            study_data = default_study_data
    if doscontents:
        content_type2, content_string2 = doscontents.split(",")
        decoded2 = base64.b64decode(content_string2)
        
        try:
            if 'csv' in dosagefilename:
                dosage_data = pd.read_csv(io.StringIO(decoded2.decode("utf-8")))
            elif 'xls' in dosagefilename:
                dosage_data = pd.read_excel(io.BytesIO(decoded2))
            
        except Exception as e:
            error_message = html.Div(
                className="alert",
                children=["That doesn't seem to be a valid dosage csv/EXCEL file!"],
            )
            error_status = True
            dosage_data = default_dos_data
            
    if anncontents:
        content_type3, content_string3 = anncontents.split(",")
        decoded3 = base64.b64decode(content_string3)
        
        try:
            if 'csv' in annfilename:
                ann_data = pd.read_csv(io.StringIO(decoded3.decode("utf-8")))
            elif 'xls' in annfilename:
                ann_data = pd.read_excel(io.BytesIO(decoded3))
            
        except Exception as e:
            error_message = html.Div(
                className="alert",
                children=["That doesn't seem to be a valid annotation csv/EXCEL file!"],
            )
            error_status = True
            ann_data = default_dos_data
    # Update Dropdown
    options = []
   # if "test_article" in study_data.columns:
    #    test_articles = study_data.test_article.unique()
     #   for test_article in test_articles:
      #      for study in study_data.study_id[
       #         study_data.test_article == test_article
        #    ].unique():
         #       options.append(
          #          {"label": f"{test_article} (study: {study})", "value": study}
           #     )
    #else:
    #    for study in study_data.study_id.unique():
    #        options.append({"label": study, "value": study})

   # options.sort(key=lambda item: item["label"])
  #  value = options[0]["value"] if options else None
    value = None

    return error_status, error_message

# Callback to generate dosage data
@app.callback(
    (Output("dosage","figure")),
    [Input("upload-dosage-data", "contents"), Input('url', 'pathname'), Input("error", "data"), Input("error-url", "data")],
    [State('upload-dosage-data', 'filename')],
)
def update_output2(contents,pathname, error, error_url, filename):
    if error or not contents:
        dosage_data = default_dos_data
    else:
        content_type, content_string = contents.split(",")
        decoded = base64.b64decode(content_string)
        if 'csv' in filename:
            dosage_data = pd.read_csv(io.StringIO(decoded.decode("utf-8")),index_col=0)
        elif 'xls' in filename:
            dosage_data = pd.read_excel(io.BytesIO(decoded),index_col=0)
    res=[]
    for col in dosage_data.columns:
        trace=go.Scatter(x=dosage_data.index,y=dosage_data[col],mode='lines+markers',name=col,text=col,)
        res.append(trace)
        
    figure=go.Figure(
            data=res,
            layout=go.Layout(
                margin=go.layout.Margin(t=0, r=50, b=50, l=50),
                yaxis=dict(title=dict(text="Fold Change")),
                xaxis=dict(title=dict(text="Passage")),
                legend=dict(orientation='h',yanchor='top',xanchor='center',y=-0.2,x=0.5),
            ),
        )
        
    return figure
    

# Callback to generate study data
@app.callback(
    [Output("plot", "figure"),Output('intermediate-value', 'children')],
    [Input("upload-data", "contents"), Input("chart-type", "value"), Input("min-prevalence", "value"), Input("display-legend", "value"),Input('url', 'pathname'), Input("error", "data"),Input("error-url", "data")],
    [State('upload-data', 'filename'), State('datatable-annotation','data')],
)
def update_output(contents, chart_type, min_prevalence, togglelegend, pathname, error, error_url, filename,  ann_table ):

    if error or not contents:
        study_data = default_study_data
    else:
        content_type, content_string = contents.split(",")
        decoded = base64.b64decode(content_string)
        if 'csv' in filename:
            study_data = pd.read_csv(io.StringIO(decoded.decode("utf-8")),index_col=0)
        elif 'xls' in filename:
            study_data = pd.read_excel(io.BytesIO(decoded),index_col=0)
        
    filterlist = []
    if chart_type == 'heatmap':
        col_filter=[]
        for col in study_data.columns:
            if all ( i < min_prevalence for i in study_data[col]):
                col_filter.append(col)
                
               
        study_data_filter = study_data.drop(col_filter,axis=1) if col_filter else study_data
        filterlist= study_data_filter.columns
        
        # pd Index is immutable.  Convert to list first
        new_columnname=study_data_filter.columns.tolist()
        
        if ann_table:
            ann_table_df = pd.DataFrame(ann_table)
            for col_index in range(0,len(new_columnname)):
                matched_ann = ann_table_df[ann_table_df['ID'].str.contains(new_columnname[col_index])]
                if not matched_ann['Product'].empty:
                    new_columnname[col_index] = new_columnname[col_index] + '<br>Product:' + matched_ann['Product'].str.cat(sep=' ')
                    
                 
        figure = go.Figure(
                   data=go.Heatmap(
                       z=study_data_filter.values.transpose(),
                       x=study_data_filter.index,
                       y=pd.Index(new_columnname),
                       colorscale='Viridis',
                       hovertemplate='Passage: %{x}<br>ID: %{y}<br>Prevalence: %{z}<extra></extra>',
                       hoverongaps = False
                   ),
                   layout=go.Layout(
                        margin=go.layout.Margin(t=0, r=50, b=50, l=50),
                        xaxis=dict(title=dict(text="Passage")),
                        yaxis=dict(showticklabels=False),
                   )
        )
        
     #   if "legendon" in togglelegend :
      #      figure.update_layout(yaxis=dict(showticklabels=True))
      #  else:
       #     figure.update_layout(yaxis=dict(showticklabels=False))    
                   
    
                   
    elif chart_type == 'line':
        res=[]
        if ann_table:
            ann_table_df = pd.DataFrame(ann_table)
        for col in study_data.columns:
            if any ( i >= min_prevalence for i in study_data[col]):
                text = col
                if ann_table:
                    matched_ann = ann_table_df[ann_table_df['ID'].str.contains(col)]
                    if not matched_ann['Product'].empty:
                        text = col + '<br>' + matched_ann['Product'].str.cat(sep=' ')
                    
                trace=go.Scatter(x=study_data.index,y=study_data[col],mode='lines+markers',name=col,text=text,customdata=[col],)
                res.append(trace)
                filterlist.append(col)
        
        figure=go.Figure(
                data=res,
                layout=go.Layout(
                    margin=go.layout.Margin(t=0, r=50, b=50, l=50),
                    yaxis=dict(title=dict(text="Prevalence")),
                    xaxis=dict(title=dict(text="Passage")), 
                    hovermode='closest',
                ),
        )
        

        if "legendon" in togglelegend :
            figure.update_layout(legend=dict(orientation='h',yanchor='top',xanchor='center',y=-0.2,x=0.5))
        else:
            figure.update_layout(showlegend=False)
      
    
        
    return figure, filterlist

# Callback to generate annotation table 
@app.callback([Output('datatable-annotation', 'data'),
               Output('datatable-annotation', 'columns')],
              [Input('intermediate-value', 'children'), Input("upload-annotation-data", "contents"),Input("error", "data"),Input("error-url", "data")],
              [State('upload-annotation-data', 'filename'), State("chart-type", "value")],
)
def update_table_output(filter_data, contents, error, error_url, filename, chart_type, ):
    if error or not contents:
        ann_data = default_annotation_data
    else:
        content_type, content_string = contents.split(",")
        decoded = base64.b64decode(content_string)
        if 'csv' in filename:
            ann_data = pd.read_csv(io.StringIO(decoded.decode("utf-8")))
        elif 'xls' in filename:
            ann_data = pd.read_excel(io.BytesIO(decoded))
            
    
    ann_data_filter = ann_data
    if filter_data:
        row_filter=[]
        for index, row in ann_data.iterrows():
            if row[0] not in filter_data:
                row_filter.append(index)
        	    
        ann_data_filter = ann_data.drop(ann_data.index[row_filter])
    
  #  if hover_data:
   #     if chart_type == 'heatmap':
    #        selected_id = hover_data['points'][0]['y']
     #   elif chart_type == 'line':
      #      id_index = hover_data['points'][0]['curveNumber']
       #     selected_id =  filter_data[id_index]
        #print(selected_id)
    #print(ann_data_filter.shape)
    return ann_data_filter.to_dict('records'), [{"name": i, "id": i} for i in ann_data_filter.columns]


if __name__ == "__main__":
    app.run_server(debug=True)
