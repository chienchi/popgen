#Grab the latest alpine image
FROM continuumio/miniconda3

# Install python and pip
RUN apt-get update --fix-missing
ADD ./requirements.txt /tmp/requirements.txt

# Install dependencies
RUN pip install --no-cache-dir -q -r /tmp/requirements.txt

# Add our code
ADD . /opt/webapp/
WORKDIR /opt/webapp

# Run the image as a non-root user
RUN groupadd myuser && useradd -m -u 2000 -g myuser myuser
USER myuser

CMD gunicorn --bind 0.0.0.0:$PORT app:server
