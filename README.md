# PopGen

## About this app

This app is implemented by [Dash](https://plot.ly/dash/) which is a python web framework 
built on top of [Plotly.js](https://plot.ly/javascript/), [React](https://github.com/facebook/react) 
and [Flask](https://palletsprojects.com/p/flask/). It displays the results of a population 
genetic study of serial bacterial passages of different antimicrobial drug treatment by 
their sub-minimum inhibitory concentration for antimicrobial drug resistant (AMR) study. 
The app helps identifying and plotting mutation kinetics in an interactive chart and table. 
Data are loaded from three csv or EXCEL result files:

* Prevalence of Mutations 

The file should have one row for each passage. Each column is a unique mutation ID 
(ex: genome accession + coordinate). The value is the prevalence in percent over 
the depth of covered position(s).

* Dosage

The file should have one row for each passage. Each column is a drug name. 
The value is the fold increase/decrease for each passage.

* Annotation

The file should have one row for each unique mutation ID (ex: genome accession + coordinate), 
corresponding to the column of prevalence of mutation table. The first column header should 
be "**ID**" And at least has a column with header "**Product**". You can import with more columns 
for the editable table and export.


## How to run this app

(The following instructions apply to Windows command line.)

To run this app first clone repository and then open a terminal to the app folder.

```
git clone https://gitlab.com:chienchi/popgen.git
cd popgen
```

Create and activate a new virtual environment (recommended) by running
the following:

On Windows

```
virtualenv venv 
\venv\scripts\activate
```

Or if using linux/Mac

```bash
python3 -m venv myvenv
source myvenv/bin/activate
```

Install the requirements:

```
pip install -r requirements.txt
```
Run the app:

```
python app.py
```
You can run the app on your browser at http://127.0.0.1:8050


## Screenshots

![demo.png](demo.png)

## Reference

Good BH, McDonald MJ, Barrick JE, Lenski RE, Desai MM. The dynamics of molecular evolution 
over 60,000 generations. [Nature. 2017 Nov 2;551(7678):45-50.](https://www.nature.com/articles/nature24287) 
doi: 10.1038/nature24287. 

